/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fibonacci;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

/**
 *
 * @author pedro
 */
public class FXMLDocumentController implements Initializable {
    
    
    private Integer fibonacci(Integer n){
       if (n<2)
        return n;
       return fibonacci(n-1) + fibonacci(n-2); 
    }
    
    @FXML
    private TextField txt_termo;
    @FXML
    private ListView<Integer> list_sequencia;
    @FXML
    private ImageView logo;
    
    ObservableList<Integer> data = FXCollections.observableArrayList();
    
    
    //##########################################################################
    // funcoes dos botoes 
    //##########################################################################
    //botao limpar
    @FXML
    private void limpar(){
        txt_termo.clear();
        list_sequencia.getItems().clear();
        
    }//end botao limpar
    
    // botao fechar
    @FXML
    private void fechar(){
        System.exit(0);
    }// end botao fechar
    
    //botao calcular
    
    
    @FXML
    private void calcular(){
        Integer termo;
       
        termo = Integer.valueOf(txt_termo.getText());
        
      
	for (int i = 0; i <= termo; i++) {
            
            data.add(fibonacci(i));
	}
        list_sequencia.setItems(data);
        
    }//end botao calcular
    
    //##########################################################################
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
